<?php

/**
 * Implementation of hook_views_preview_info_alter.
 */
function views_explain_views_preview_info_alter(&$rows, $view) {
  $explain = db_fetch_array(db_query("EXPLAIN " . $view->build_info['query'], $view->build_info['query_args']));
  foreach ($explain as $key => $val) {
    $rows[] = array('<strong>' . ucfirst($key) . '</strong>', $val);
  }
}


